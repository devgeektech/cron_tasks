var async = require('async');
var queue = require('./queue');
var db = require('../model');


exports.create = function (req, res) {
    async.waterfall([
        function (cb) {
            db.Task
                .build({
                    client_id: req.body.client_id,
                    cmd_name: req.body.cmd_name,
                    vhost: req.body.vhost,
                    app: req.body.app
                })
                .save()
                .then(function(task) {
                    cb(null, task);
                }).catch(function(error) {
                    return cb(error);
            });
        },
        function (task, cb) {
            queue.send(task, function (err) {
                if(err)
                    return cb(err);
                cb(null, task);
            })
        }
    ], function (err, task) {
        if(err) {
            return res.json({ error: err})
        }
        res.json({ id: task.id})
    });

};
exports.list = function (req, res) {
    db.Task
        .findAndCountAll({
            where: {
                status: 'waiting'
            }
        })
        .then(function(result) {
            res.json(result.rows);
        }).catch(function(error) {
            res.json({
                error: error
            })
        });
};

exports.next = function (req, res) {
    var nextTask;
    async.waterfall([
        function (cb) {
            db.queueTask
                .findAndCountAll()
                .then(function(result) {
                    cb(null, result.rows);
                }).catch(function(error) {
                return cb(error);
            });
        },
        function (nextTask, cb) {
            if(nextTask.length == 0) {
                return cb('no tasks in the queue right now')
            }
            db.Task
                .findOne({
                    where: {
                        id: nextTask[0].taskId
                    }
                })
                .then(function(result) {
                    cb(null, result);
                }).catch(function(error) {
                    cb(error);
                });
        }
    ], function (err, nextTask) {
        if(err) {
            return res.json({ error: err});
        }
        res.json({
            data: nextTask
        });
    })
};


exports.update = function (req, res) {
    var body = req.body;
    async.waterfall([
        function(cb) {
            db.Task.find({
                where: { id: parseInt(body.task_id, 10) }
            }).then(function(task) {
                task.updateAttributes({
                    exec_end_time: body.end_time,
                    exec_exit_code: body.exit_code,
                    output: body.output
                }).then(function(taskModel) {
                    cb(null)
                }).catch(function(error) {
                    return cb(error);
                })
            })
        },
        function(cb) {
            db.queueTask.destroy({
                where: { taskId: parseInt(body.task_id, 10) }
            }).then(function (affectedRows) {
                cb(null)
            })
        }
    ], function (err) {
        if(err) {
            return res.json({ error: err});
        }
        res.json({});
    })
};