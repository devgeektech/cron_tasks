var aws = require('aws-sdk');
aws.config.loadFromPath(__dirname + './../config.json');
var async = require('async');
var model = require('../model/Tasks');
var sqs = new aws.SQS();

var queueUrl = "https://sqs.us-west-2.amazonaws.com/637027676455/enbake-queue";


exports.create = function (task, callback) {
    var params = {
        QueueName: task.id.toString()
    };

    sqs.createQueue(params, function(err, data) {
        if(err) {
            callback(err);
        }
        else {
            callback(null, data);
        }
    });
};


exports.queuelist  = function (callback) {
    sqs.listQueues(function(err, data) {
        if(err)
            callback(err);
        else
            callback(null, data);
    });
};



exports.send = function (task, callback) {
    var params = {
        MessageBody: task.id.toString(),
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };

    sqs.sendMessage(params, function(err, data) {
        if(err) {
            callback(err);
        }
        else {
            callback(null, data);
        }
    });
};

exports.recieve = function (url, callback) {
    var params = {
        QueueUrl: url,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err)
            callback(err);
        else
            callback(null, data);
    });
};




var purge = function (callback) {
    var params = {
        QueueUrl: queueUrl
    };

    sqs.purgeQueue(params, function(err, data) {
        if(err) {
            callback(err);
        }
        else {
            callback(null, data);
        }
    });
};

exports.delete = function (req, res) {
    var params = {
        QueueUrl: queueUrl,
        ReceiptHandle: receipt
    };

    sqs.deleteMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        }
        else {
            res.send(data);
        }
    });
};

exports.deleteQueue = function (req, res) {
    var params = {
        QueueUrl: queueUrl /* required */
    };
    sqs.deleteQueue(params, function(err, data) {
        if(err) {
            res.send(err);
        }
        else {
            res.send(data);
        }        // successful response
    });
};



var cron = function () {
    var tasks = [];
    async.waterfall([
        function (cb) {
            queuelist(function (err, queueList) {
                if(err)
                    return cb(err);
                else
                    cb(null, queueList);
            });
        },
        function (queueList, cb) {
            async.eachSeries(queueList.QueueUrls, function (singleQueue, next) {
                var flag = true;
                var getAllMessages = function () {
                    recieve(singleQueue, function (err, recievedData) {
                        if(recievedData.Messages) {
                            tasks.push(recievedData.Messages[0].Body);
                            getAllMessages()
                        }else {
                            flag = false;
                            next();
                        }
                    })
                };
                getAllMessages();
            }, function () {
                cb(null);
            })
        },
        function (cb) {
            if((!tasks) || (tasks.length == 0)) {
                return cb('tasks are performed, no other tasks to perform');
            }
            async.eachSeries(tasks, function (task, next) {
                model.Task.find({
                    where: { id: parseInt(task, 10) }
                }).then(function(task) {
                    task.updateAttributes({
                        exec_start_time: Date.now(),
                        status: 'running'
                    }).then(function(taskModel) {
                        next();
                    }).catch(function(error) {
                        return cb(error);
                    })
                })

            }, function () {
                cb(null);
            })
        },
        function (cb) {
            // working of tasks
            cb(null);
        },
        function (cb) {
            async.eachSeries(tasks, function (task, next) {
                model.Task.find({
                    where: { id: parseInt(task, 10) }
                }).then(function(task) {
                    task.updateAttributes({
                        exec_end_time: Date.now(),
                        status: 'complete'
                    }).then(function(taskModel) {
                        next();
                    }).catch(function(error) {
                        return cb(error);
                    })
                })

            }, function () {
                cb(null);
            })
        }
    ], function (err) {
        if(err)
            return console.log(err);
        purge(function () {
            console.log('task Complete')
        })
    });
};



