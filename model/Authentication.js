module.exports = function (sequelize, DataTypes) {
    var Authentication = sequelize.define('Authentication', {
        uuid: {
            type: DataTypes.STRING,
            unique: true
        },
        secret_key: {
            type: DataTypes.STRING,
            unique: true
        },
        description: {
            type: DataTypes.STRING
        }
    });
    return Authentication;
};
