module.exports = function (sequelize, DataTypes) {
    var Server = sequelize.define('Server', {
        uuid: {
            type: DataTypes.STRING,
            unique: true
        },
        secret_key: {
            type: DataTypes.STRING,
            unique: true
        },
        hostname: {
            type: DataTypes.STRING
        }
    });
    return Server;
};
