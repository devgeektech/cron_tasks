

module.exports = function (sequelize, DataTypes) {
    var Task = sequelize.define('Task', {
        client_id: {
            type: DataTypes.STRING
        },
        cmd_name: {
            type: DataTypes.STRING
        },
        vhost: {
            type: DataTypes.STRING
        },
        app: {
            type: DataTypes.STRING
        },
        status: {
            type: DataTypes.STRING
        },
        exec_start_time: {
            type: DataTypes.DATE
        },
        exec_end_time: {
            type: DataTypes.DATE
        },
        exec_exit_code: {
            type: DataTypes.STRING
        },
        output: {
            type: DataTypes.STRING
        }
    });
    return Task;
};

