var db = require('../model');

exports.requiresAuthentication = function(req, res, next) {
    var secretKey = req.headers['x-secret-key'];
    var udid = req.headers['x-key-id'];
    if(!secretKey || !udid) {
        return res.status(403).send({
            success: false,
            message: 'not authenticated request'
        });
    }
    db.Authentication.find({
        where: {
            uuid: udid,
            secret_key: secretKey
        }
    })
        .then(function (server) {
            if(!server)
                return res.json({ success: false, message: 'Failed to authenticate request' });
            next();
        })

};

