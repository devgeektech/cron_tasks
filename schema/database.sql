-- --------------------------------------------------------
-- Host:                         mysql.cut192bompxe.us-west-2.rds.amazonaws.com
-- Server version:               5.6.27-log - MySQL Community Server (GPL)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.3.0.5117
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for rootdb
CREATE DATABASE IF NOT EXISTS `rootdb` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rootdb`;

-- Dumping structure for table rootdb.Authentication
CREATE TABLE IF NOT EXISTS `Authentication` (
  `uuid` char(36) NOT NULL,
  `secret_key` char(50) NOT NULL,
  `description` varchar(256) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_update` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table rootdb.queueTasks
CREATE TABLE IF NOT EXISTS `queueTasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table rootdb.Servers
CREATE TABLE IF NOT EXISTS `Servers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) DEFAULT NULL,
  `secret_key` varchar(255) DEFAULT NULL,
  `hostname` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `secret_key` (`secret_key`),
  UNIQUE KEY `Servers_uuid_unique` (`uuid`),
  UNIQUE KEY `Servers_secret_key_unique` (`secret_key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table rootdb.Tasks
CREATE TABLE IF NOT EXISTS `Tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(255) DEFAULT NULL,
  `cmd_name` varchar(255) DEFAULT NULL,
  `vhost` varchar(255) DEFAULT NULL,
  `app` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `exec_start_time` datetime DEFAULT NULL,
  `exec_end_time` datetime DEFAULT NULL,
  `exec_exit_code` datetime DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
