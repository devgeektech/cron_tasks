var queue = require('../controller/queue');
var tasks = require('../controller/tasks');
var tasksAuth = require('../middleware/tasksAuthorization');
var queueAuth = require('../middleware/queueAuthorization');



module.exports.configureRoutes = function(app) {
// main Api render //
    app.get('/', function (req, res) {
        res.render('index');
    });


// queue apis//

    app.get('/queue/create', queue.create);
    app.get('/queue/delete', queue.delete);
    app.get('/queue/deleteQueue', queue.deleteQueue);
    // app.get('/queue/all', queue.all);
    // app.get('/queue/send', queue.send);
    // app.get('/queue/recieve', queue.recieve);
    // app.get('/queue/purge', queue.purge);
    // app.get('/queue/process', queue.process);


// tasks api //

    app.post('/tasks', tasksAuth.requiresAuthentication, tasks.create);   // tasksAPI
    app.get('/tasks', tasksAuth.requiresAuthentication, tasks.list);   //tasksAPI
    app.get('/tasks/next', queueAuth.requiresAuthentication, tasks.next);    // QueueAPI
    app.put('/tasks', queueAuth.requiresAuthentication, tasks.update);    // QueueAPI

};