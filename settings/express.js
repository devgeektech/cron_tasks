var express = require('express');
var path = require('path');
var config = require('./config');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

module.exports.configure = function(app) {
    app.use(logger('combined'));
    app.use(bodyParser.json());
    app.use(cookieParser());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    var root = path.normalize(__dirname + '../../');
    config.root = root;
    app.set('views', path.join(root, 'views'));
    app.set('view engine', 'ejs');
    app.use(express.static(path.join(root, 'public')));
};