var config = require('./config');
var Sequelize = require('sequelize');

var model = new Sequelize(config.dbServer.dbName, config.dbServer.user, config.dbServer.password, {
    host: config.dbServer.host,
    port: config.dbServer.port,
    dialect: config.dbServer.dialect
});
model
    .authenticate()
    .then(function(err) {
        console.log('Connection has been established successfully.');
    })
    .catch(function (err) {
        console.log('Unable to connect to the database:', err);
    });

exports.model = model;


