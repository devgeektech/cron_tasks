var aws = require('aws-sdk');
aws.config.loadFromPath(__dirname + '/../config.json');
var async = require('async');
var db = require('../model');
var sqs = new aws.SQS();

var queueUrl = "https://sqs.us-west-2.amazonaws.com/637027676455/enbake-queue";


exports.create = function (task, callback) {
    var params = {
        QueueName: task.id.toString()
    };

    sqs.createQueue(params, function(err, data) {
        if(err) {
            callback(err);
        }
        else {
            callback(null, data);
        }
    });
};


var queuelist  = function (callback) {
    sqs.listQueues(function(err, data) {
        if(err)
            callback(err);
        else
            callback(null, data);
    });
};



exports.send = function (task, callback) {
    var params = {
        MessageBody: task.id.toString(),
        QueueUrl: queueUrl,
        DelaySeconds: 0
    };

    sqs.sendMessage(params, function(err, data) {
        if(err) {
            callback(err);
        }
        else {
            callback(null, data);
        }
    });
};

var recieve = function (url, callback) {
    var params = {
        QueueUrl: url,
        VisibilityTimeout: 600 // 10 min wait time for anyone else to process.
    };

    sqs.receiveMessage(params, function(err, data) {
        if(err)
            callback(err);
        else
            callback(null, data);
    });
};




var purge = function (callback) {
    var params = {
        QueueUrl: queueUrl
    };

    sqs.purgeQueue(params, function(err, data) {
        if(err) {
            callback(err);
        }
        else {
            callback(null, data);
        }
    });
};

exports.delete = function (req, res) {
    var params = {
        QueueUrl: queueUrl,
        ReceiptHandle: receipt
    };

    sqs.deleteMessage(params, function(err, data) {
        if(err) {
            res.send(err);
        }
        else {
            res.send(data);
        }
    });
};

exports.deleteQueue = function (req, res) {
    var params = {
        QueueUrl: queueUrl /* required */
    };
    sqs.deleteQueue(params, function(err, data) {
        if(err) {
            res.send(err);
        }
        else {
            res.send(data);
        }        // successful response
    });
};


var getTasks = function () {
    async.waterfall([
        function (cb) {
            queuelist(function (err, queueList) {
                if(err)
                    return cb(err);
                else
                    cb(null, queueList);
            });
        },
        function (queueList, cb) {
            async.eachSeries(queueList.QueueUrls, function (singleQueue, next) {
                var getAllMessages = function () {
                    recieve(singleQueue, function (err, recievedData) {
                        if(recievedData.Messages) {
                            db.queueTask
                                .build({
                                    taskId: parseInt(recievedData.Messages[0].Body, 10)
                                })
                                .save()
                                .then(function(task) {
                                    console.log('created');
                                    getAllMessages();
                                }).catch(function(error) {
                                return cb(error);
                            });

                        }else {
                            next();
                        }
                    })
                };
                getAllMessages();
            }, function () {
                cb(null);
            })
        }
    ], function (err) {
        if(err)
            return console.log(err);
        purge(function () {
            console.log('queue purged')
        })
    })
};


var cron = function () {
    var tasks = [];
    async.waterfall([
        function (cb) {
            db.queueTask
                .findAndCountAll()
                .then(function(result) {
                    cb(null, result.rows);
                }).catch(function(error) {
                return cb(error);
            });
        },
        function (queueList, cb) {
            if((!queueList) || (queueList.length == 0)) {
                return cb('tasks are performed, no other tasks to perform');
            }
            async.eachSeries(queueList, function (queueItem, next) {
                db.Task.find({
                    where: { id: queueItem.taskId }
                }).then(function(task) {
                    task.updateAttributes({
                        exec_start_time: Date.now(),
                        status: 'running'
                    }).then(function(taskModel) {
                        db.queueTask.destroy({
                            where: {
                                id: queueItem.id
                            }
                        }).then(function (affectedRows) {
                            next();
                        })
                    }).catch(function(error) {
                        return cb(error);
                    }).catch(function(error) {
                        return cb(error);
                    })
                })

            }, function () {
                cb(null, queueList);
            })
        },
        function (queueList, cb) {
            // working of tasks
            cb(null, queueList);
        },
        function (queueList, cb) {
            async.eachSeries(queueList, function (queueItem, next) {
                db.Task.find({
                    where: { id: queueItem.taskId }
                }).then(function(task) {
                    task.updateAttributes({
                        exec_end_time: Date.now(),
                        status: 'complete'
                    }).then(function(taskModel) {
                        db.queueTask.destroy({
                            where: {
                                id: queueItem.id
                            }
                        }).then(function (affectedRows) {
                            next();
                        })
                    }).catch(function(error) {
                        return cb(error);
                    }).catch(function(error) {
                        return cb(error);
                    })
                })

            }, function () {
                cb(null);
            })
        }
    ], function (err) {
        if(err)
            return console.log(err);
        purge(function () {

            console.log('task Complete')
        })
    });
};


// setInterval(function() {
//     console.log('start performing');
//     cron();
// },  5* 60 * 1000);


setInterval(function() {
    console.log('start getting queue tasks');
    getTasks();
},  60 * 1000);
