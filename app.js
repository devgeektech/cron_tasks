var express = require('express');
var http = require('http');
var config = require('./settings/config');
var database = require('./settings/database');
require('./model/Tasks');
require('./model/queueTasks');
var db = require('./model');
var app = express();
require('./settings/express').configure(app);
require('./settings/routes').configureRoutes(app);




var server = http.createServer(app);
var port = config.webServer.port || 3000;
function startApp() {
    server.listen(port, function () {
        console.log('task runner ready');
    });
}

db.sequelize.sync()
    .then(startApp)
    .catch(function (e) {
        throw new Error(e);
    });


exports.module = exports = app;